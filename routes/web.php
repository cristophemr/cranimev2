<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

/**
 * Rutas principales del sistema
 */
Route::get('/', 'FrontController@index')->name("index");
Route::get('/lista', 'FrontController@lista')->name("catalog");
Route::get('anime/{slug}', 'FrontController@anime')->name("anime");
Route::get('/privacidad', "FrontController@privacy")->name("privacy");
Route::get('terminos', "FrontController@terms")->name("terms");


/**
 * SISTEMA
 */
Route::post("/api", "BotController@api");
Route::post("/chapter", "BotController@getChapters");
Route::post("/search", "FrontController@Buscador")->name("search");
Route::get("/search", "FrontController@Buscador")->name("search");


/**
 * Get Images
 */
Route::get('images/{filename}', function ($filename) {

    $filename = str_replace("*", "/", $filename);
    if (strpos($filename, "/") == true) {
        $path = storage_path() . '/app/public/' . $filename;
    } else {
        $path = storage_path() . '/app/public/images/' . $filename;
    }

    if (!File::exists($path)) abort(404);

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
})->name("images");
Route::get('vtt/{filename}', function ($filename) {

    $filename = str_replace("*", "/", $filename);
    if (strpos($filename, "/") == true) {
        $path = storage_path() . '/app/public/' . $filename;
    } else {
        $path = storage_path() . '/app/public/images/' . $filename;
    }

    if (!File::exists($path)) abort(404);

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", "text/vtt")->header("access-control-allow-origin", "*")->header("access-control-allow-methods", "GET, POST, OPTIONS");

    return $response;
})->name("vtt");

