<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;


class anime extends Model
{
    protected $table = "animes";
    protected $hidden = ["id", "id_type", "created_at", "updated_at", "visible", "source", "state", "sinopsis", "date", "img_seo"];

    public function Categories()
    {
        return $this->belongsToMany(category::class);
    }

    public function Type()
    {
        return $this->hasOne(type::class, 'id', 'id_type');
    }

    public function charters()
    {
        return $this->hasMany(charter::class, "id_anime", "id");
    }

    /**
     * Scopes Filter
     */
    public function scopeName($query, $name)
    {
        if ($name)
            return $query->where('name', 'LIKE', "%$name%");
    }

    public function scopeState($query, $state)
    {
        if ($state)
            return $query->where('state', $state);
    }

    public function scopeTyp($query,$Type)
    {
            if($Type)
                return  $query->where('id_type', $Type);
    }
    public function scopeVisible($query)
    {
                return  $query->where('visible','<>', 0);
    }
    public function scopeCat($query,$Cat)
    {
        if($Cat){
            dd($query->with('Categories')
                //->where('categories.category', $Cat)
                ->get());
            return $query->whereIn('category', $Cat)
                ->with('Categories')
                ->get();
//            return $query->with(array('Categories' => function($query) use($Cat)
//            {
//                return $query->where('category',"LIKE", "%$Cat%");
//            }));
        }
    }

}
