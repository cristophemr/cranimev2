<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class charter extends Model
{
    public function anime()
    {
        return $this->belongsTo(anime::class,"id",'id_anime');
    }
    public function server()
    {
        return $this->belongsToMany(server::class,"charters_source")->withPivot('code')->withPivot('vtt_source')->withTimestamps();
    }
}
