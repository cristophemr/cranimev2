<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class server extends Model
{
    public function charter()
    {
        return $this->belongsToMany(charter::class, "charters_source")->withPivot('code','status')->withTimestamps();
    }
    public function code($code){
        return $this->belongsToMany(charter::class, "charters_source")
            ->withPivot("code")
            ->where("code", "=",$code)->get();
    }
}
