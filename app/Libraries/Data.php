<?php

namespace App\Libraries;


use CloudflareBypass\RequestMethod\CFStream;

class Data
{

    public function CFIMAGES($url)
    {
        $stream_cf_wrapper = new CFStream(array(
            'max_retries' => 5,
            'cache' => false,
            'cache_path' => __DIR__ . '/cache',
            'verbose' => false
        ));
        $opts = array(
            'http' => array(
                'method' => "GET",
                'header' => array(
                    'accept: */*',       // required
                    'host: animeflv.net',    // required
                    'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36'
                )
            )
        );
        $ctx = $stream_cf_wrapper->contextCreate($url, stream_context_create($opts));
        $img = file_get_contents($url, false, $ctx);

        return $img;
    }

    function CFIMAGENAME($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString.'.jpg';
    }
    function get_Cover($param, $html){
        $data = explode($param,$html);
        $data = $data[6];
        return $data;
    }

}
