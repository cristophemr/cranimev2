<?php

namespace App\Http\Controllers;

use App\models\type;
use function Couchbase\defaultDecoder;
use Illuminate\Http\Request;
use App\models\anime;
use App\models\category;
use App\models\charter;
use Illuminate\Support\Facades\Input;

class FrontController extends Controller
{
    public function __construct()
    {
    }

    public function index()
    {
        $Slides = $this->slides();
        $Last = anime::orderBy('date', 'desc')->select('id_type', 'name', 'sinopsis', 'img', 'img_seo', 'slug', 'state')->where("visible", "<>", 0)->take(12)->get();
        $Top = "";//anime::orderBy('vista', 'desc')->select('id','name','sinopsis','img','img_seo','slug')->take(12)->get();
        return view("front.index", compact("Slides", "Top", "Last"));
    }

    public function anime($slug)
    {
        $Slug = str_slug($slug, "-");
        $Anime = anime::where([['slug', $Slug], ["visible", "<>", 0]])->firstorfail();
        return view("front.anime", compact("Anime"));
    }

    public function lista(Request $request)
    {
        $Generos = category::all();
        $Tipos = type::all();

        $Genero = "";
        $Estado = "";
        $Tipo = "";

        $Genero = $request->get("gender");
        $Estado = $request->get("state");
        $Tipo = $request->get("type");

        switch ($Estado) {
            case "Emisión":
                $Estado = 0;
                break;
            case "Concluido":
                $Estado = 1;
                break;
            default:
                $Estado = "";
                break;
        }
        if($Tipo == "Todos")
        {
            $Tipo = "";
        }
            $Animes = anime::orderBy('state', 'ASC')
                ->Cat($Genero)
//                ->Typ($this->getType($Tipo))
//                ->State($Estado)
                ->Visible()
                ->paginate(30)
            ;
        $Estado = ($Estado == 1) ? "Concluido" : "Emisión";

        return view("front.catalogo", compact("Generos", "Tipos", "Animes","Genero","Estado","Tipo"));
    }

    public function slides()
    {
        $Slides = charter::select("id_anime")->orderBy('id', 'desc')->take(10)->get()->unique('id_anime');
        $array = [];

        foreach ($Slides as $slide => $value) {
            array_push($array, anime::select('id', 'name', 'img', 'img_seo', 'slug')->where("id", "=", $value->id_anime)->take(10)->get());
        }

        return $array;
    }

    public function buscador(Request $request)
    {
        $Animes = "";
        if(is_null($request->media))
        {
            return view("front.search", compact("Animes"));
        }else
        {
            $Animes = anime::where([["visible", "<>", 0], ["name", "like", "%" . $request->media . "%"]])->paginate(70);
            return view("front.search", compact("Animes"));

        }
    }

    public function privacy()
    {
        $Title = "Terminos de Privacidad";

        return view("front.text", compact("Title"));
    }

    public function terms()
    {
        $Title = "Terminos de uso";
        $Content = "";
        return view("front.text", compact("Title"));
    }
    public function getType($Type)
    {
        $result = type::where("type","LIKE","%$Type%")->pluck("id");
        return $result[0];
    }
    public function getGender($Gender)
    {
        $result = category::where("type","LIKE","%$Gender%")->pluck("id");
        return $result[0];
    }
}
