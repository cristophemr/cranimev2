<?php

namespace App\Http\Controllers;


use Intervention\Image\File;

class MediaController extends Controller
{
    public function getVideo($code)
    {
        $filePath = $code;
        $response = fopen($filePath, "r");
        while (!feof($response)) {
            // Read 1024 bytes from the stream
            echo fread($response, 2048);
        }
        sleep(1800);
        fclose($response);
        return response($response)->header("Content-Type", 'video/mp4');
    }

}
