<?php

namespace App\Http\Controllers;

use App\Libraries\Data;
use App\models\category;
use App\models\charter;
use Illuminate\Http\Request;
use Goutte\Client;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\models\anime;

class BotController extends Controller
{
    public function api(Request $request)
    {
        try {
            /*Guardado inicial*/
            foreach ($request->Animes as $item) {
                $value = $this->VerifyAnime($item["NombreAnime"]);
                if ($value == 0) {
                    //Almacenamiento
                    $Anime = new anime();
                    $Anime->id_type = $this->getType($item["Tipo"]);
                    $Anime->name = $item["NombreAnime"];
                    $Anime->slug = $item["NombreURL"];
                    $Anime->sinopsis = $item["Descripcion"];
                    $Anime->img = $this->saveimg($item["Imagen"], false);
                    $Anime->img_seo = $this->saveimg($item["Imagen"], true);
                    $Anime->date = date(now());
                    $Anime->Source = $item["Link"];
                    $Anime->visible = true;
                    $Anime->state = ($item["Estado"] == "Finalizado") ? 1 : 0;
                    $Anime->save();
                    $Generos = $this->getCategories(explode(",", $item["Generos"]));
                    $Anime->Categories()->sync($Generos);
                    foreach ($item["Capitulos"] as $capitulo => $value) {

                        //Almacenamiento de Episodios
                        $Char = new charter();
                        $Char->id_anime = $Anime->id;
                        $Char->cap = $capitulo;
                        $Char->save();
                        foreach ($value as $cap => $code) {
                            DB::table('charters_source')->insert(
                                [
                                    [
                                        "charter_id" => $Char->id,
                                        "server_id" => $cap,
                                        "code" => $code,
                                        "status" => true
                                    ]
                                ]
                            );
                        }
                    }

                } elseif ($value == -1) {

                    $Anime = anime::where("name", "=", $item["NombreAnime"])->get();
                    anime::where("id", $Anime[0]["id"])->update(['state' => ($item["Estado"] == "Finalizado") ? 1 : 0]);
                    foreach ($item["Capitulos"] as $capitulo => $value) {
                        $Capitulo = charter::where("id_anime", $Anime[0]["id"])->orderBy('id', 'desc')->take(1)->get();
                        if ($Capitulo[0]["cap"] < $capitulo || is_null($Capitulo[0]["cap"]) == true) {
                            //Almacenamiento de Episodios
                            $Char = new charter();
                            $Char->id_anime = $Anime[0]["id"];
                            $Char->cap = $capitulo;
                            $Char->save();
                            foreach ($value as $cap => $code) {
                                DB::table('charters_source')->insert(
                                    [
                                        [
                                            "charter_id" => $Char->id,
                                            "server_id" => $cap,
                                            "code" => $code,
                                            "status" => true
                                        ]
                                    ]
                                );
                            }
                        }
                    }
                }

            }
        } catch (Exception $e) {

        }
    }

    public function getType($string)
    {
        $string_Type = $string;
        switch (ltrim($string_Type)) {
            case "tv":
                return 1;
                break;
            case "ova":
                return 2;
                break;
            case "movie":
                return 3;
                break;
            case "special":
                return 4;
                break;
            Default:
                return 5;
                break;
        }
    }

    public function VerifyAnime($Anime)
    {
        $Flag = 0;
        if (anime::where('name', '=', $Anime)->count() > 0) {
            $Flag = 1;
        }
        if (anime::where([['name', '=', $Anime], ['state', "=", 0]])->count() > 0) {
            $Flag = -1;
        }

        return $Flag;
    }

    public function saveimg($img, $cover)
    {
        $file = new Data();
        $info = $file->CFIMAGENAME();
        $Ruta = "public/images/" . $info;
        if ($cover == true) {
            $Name_Cover = $file->get_Cover("/",$img);
            $url = "https://animeflv.net/uploads/animes/covers/".$Name_Cover;
            Storage::disk('local')->put($Ruta, $file->CFIMAGES($url));
        } else {
            Storage::disk('local')->put($Ruta, $file->CFIMAGES($img));
        }
        return $info;
    }

    public function getCategories($array)
    {
        // dd($array);
        $Cat = [];

        for ($i = 0; $i < count($array); $i++) {
            $Busqueda = substr($array[$i], 0, 4);

            if (category::where('category', 'like', $Busqueda . "%")->count() == 0) {
                $fail = new category();
                $fail->category = $array[$i];
                $fail->save();
            }
            $Category = category::where('category', 'like', $Busqueda . "%")
                ->first();
            $Cat[$i] = $Category->id;
        }
        return $Cat;
    }

    public function getChapters(Request $request)
    {
        $anime = $request->Anime;
        if (anime::where([["name", "=", $anime], ["state", 0]])->count() > 0) {
            $Anime = anime::where([["name", "=", $anime], ["state", 0]])->get();
            $Caps = charter::where("id_anime", $Anime[0]["id"])->orderBy('id', 'desc')->take(1)->get();
            return $Caps[0]["cap"];
        } else {
            return response(-1, 200);
        }
    }

}
