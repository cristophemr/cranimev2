@extends("layouts.flixgo")

@section("slider")
    <section class="home">
        <!-- home bg -->
        <div class="owl-carousel home__bg">
            @foreach($Slides as  $Slide => $value)
            <div class="item home__cover" data-bg="{{route("images",str_replace("\\","*",$value[0]->img_seo))}}"></div>
            @endforeach
        </div>
        <!-- end home bg -->

        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1 class="home__title"><b>Emisiones</b> mas recientes</h1>

                    <button class="home__nav home__nav--prev" type="button">
                        <i class="icon ion-ios-arrow-round-back"></i>
                    </button>
                    <button class="home__nav home__nav--next" type="button">
                        <i class="icon ion-ios-arrow-round-forward"></i>
                    </button>
                </div>

                <div class="col-12">
                    <div class="owl-carousel home__carousel">
                        @foreach($Slides as $Slide)
                        <div class="item">
                            <!-- card -->
                            <div class="card card--big">
                                <div class="card__cover">
                                    <img src="{{route("images",str_replace("/","*",$Slide[0]->img))}}" alt="">
                                    <a href="{{route("anime", $Slide[0]->slug)}}" class="card__play">
                                        <i class="icon ion-ios-play"></i>
                                    </a>
                                </div>
                                <div class="card__content">
                                    <h3 class="card__title"><a href="#">{{$Slide[0]->name}}</a></h3>
                                    <span class="card__category">
										{{--<a href="#">Action</a>--}}
										{{--<a href="#">Triler</a>--}}
									</span>
                                    <span class="card__rate"><i class="icon ion-ios-star"></i>8.4</span>
                                </div>
                            </div>
                            <!-- end card -->
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section("content")
    <div class="content__head">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <!-- content tabs nav -->
                    <ul class="nav nav-tabs content__tabs" id="content__tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#tab-1" role="tab" aria-controls="tab-1" aria-selected="true">Nuevos de la temporada</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tab-2" role="tab" aria-controls="tab-2" aria-selected="false">Top 10</a>
                        </li>

                    </ul>
                    <!-- end content tabs nav -->

                    <!-- content mobile tabs nav -->
                    <div class="content__mobile-tabs" id="content__mobile-tabs">
                        <div class="content__mobile-tabs-btn dropdown-toggle" role="navigation" id="mobile-tabs" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <input type="button" value="New items">
                            <span></span>
                        </div>

                        <div class="content__mobile-tabs-menu dropdown-menu" aria-labelledby="mobile-tabs">
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="nav-item"><a class="nav-link active" id="1-tab" data-toggle="tab" href="#tab-1" role="tab" aria-controls="tab-1" aria-selected="true">Nuevos de la temporada</a></li>

                                <li class="nav-item"><a class="nav-link" id="2-tab" data-toggle="tab" href="#tab-2" role="tab" aria-controls="tab-2" aria-selected="false">Top 10</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- end content mobile tabs nav -->
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <!-- content tabs -->
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="tab-1" role="tabpanel" aria-labelledby="1-tab">
                <div class="row">
                    @foreach($Last as $item)
                    <!-- card -->
                    <div class="col-6 col-sm-12 col-lg-6">
                        <div class="card card--list">
                            <div class="row">
                                <div class="col-12 col-sm-4">
                                    <div class="card__cover">
                                        <img src="{{route("images", str_replace("/","*",$item->img))}}" alt="">
                                        <a href="{{route("anime", $item->slug)}}" class="card__play">
                                            <i class="icon ion-ios-play"></i>
                                        </a>
                                    </div>
                                </div>


                                <div class="col-12 col-sm-8">
                                    <div class="card__content">
                                        <h3 class="card__title"><a href="{{route("anime", $item->slug)}}">{{$item->name}}</a></h3>
                                        <div class="card__wrap">
                                            {{--<span class="card__rate"><i class="icon ion-ios-star"></i>8.4</span>--}}
                                            <ul class="card__list">
                                                <li>{{$item->state > 0 ? "Finalizado" : "Emision"}}</li>
                                                <li>{{$item->Type->type}}</li>
                                            </ul>
                                        </div>

                                        <div class="card__description">
                                            <p>{{$item->sinopsis}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end card -->
                    @endforeach
                </div>
            </div>

            <div class="tab-pane fade" id="tab-2" role="tabpanel" aria-labelledby="2-tab">
                <div class="row">
                    {{--@foreach($Top as $item)--}}
                    {{--<!-- card -->--}}
                    {{--<div class="col-6 col-sm-4 col-lg-3 col-xl-2">--}}
                        {{--<div class="card">--}}
                            {{--<div class="card__cover">--}}
                                {{--<img src="img/covers/cover.jpg" alt="">--}}
                                {{--<a href="#" class="card__play">--}}
                                    {{--<i class="icon ion-ios-play"></i>--}}
                                {{--</a>--}}
                            {{--</div>--}}
                            {{--<div class="card__content">--}}
                                {{--<h3 class="card__title"><a href="#">{{$item->nombre}}</a></h3>--}}
                                {{--<span class="card__category">--}}
										{{--<a href="#">Action</a>--}}
										{{--<a href="#">Triler</a>--}}
									{{--</span>--}}
                                {{--<span class="card__rate"><i class="icon ion-ios-star"></i>8.4</span>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<!-- end card -->--}}
                    {{--@endforeach--}}
                </div>
            </div>

        </div>
        <!-- end content tabs -->
    </div>
@endsection
