@extends("layouts.flixgo")
@section("title")
    {{$Anime->name}}
@endsection
@section("meta")
    {{--METAS PARA FACEBOOK--}}
    <meta property="fb:app_id"             content="1977598625883047"/>
    <meta property="og:url"                content="https://www.crani.me/anime/"{{$Anime->slug}} />
    <meta property="og:type"               content="article" />
    <meta property="og:title"              content="{{$Anime->name}}" />
    <meta property="og:description"        content="{{substr($Anime->sinopsis,0,50)}}" />
    <meta property="og:image"              content="{{route("images",str_replace("/","*",$Anime->img))}}" />
    <meta property="og:image:width"        content="200" />
    <meta property="og:image:height"       content="200" />
    <meta property="og:site_name"          content="{!! setting('site.author') !!}" />
@endsection
@section("content")
    <!-- details -->
    <section class="section details">
        <!-- details background -->
        <div class="details__bg" data-bg="{{route("images",str_replace("/","*",$Anime->img_seo))}}"></div>
        <!-- end details background -->

        <!-- details content -->
        <div class="container">
            <div class="row">
                <!-- title -->
                <div class="col-12">
                    <h1 class="details__title">{{$Anime->name}}</h1>
                </div>
                <!-- end title -->

                <!-- content -->
                <div class="col-10">
                    <div class="card card--details card--series">
                        <div class="row">
                            <!-- card cover -->
                            <div class="col-12 col-sm-4 col-md-4 col-lg-3 col-xl-3">
                                <div class="card__cover">
                                    <img src="{{route("images",str_replace("/","*",$Anime->img))}}" alt="">
                                </div>
                            </div>
                            <!-- end card cover -->

                            <!-- card content -->
                            <div class="col-12 col-sm-8 col-md-8 col-lg-9 col-xl-9">
                                <div class="card__content">
                                    <div class="card__wrap">
                                        <span class="card__rate"><i class="icon ion-ios-star"></i>8.4</span>

                                        <ul class="card__list">
                                            <li>{{$Anime->state > 0 ? "Finalizado" : "Emision"}}</li>
                                            <li>{{$Anime->Type->type}}</li>
                                            <li>
                                                {{--BOTON LIKE FACEBOOK--}}
                                                <div id="fb-root"></div>
                                                <script>(function(d, s, id) {
                                                        var js, fjs = d.getElementsByTagName(s)[0];
                                                        if (d.getElementById(id)) return;
                                                        js = d.createElement(s); js.id = id;
                                                        js.src = 'https://connect.facebook.net/es_ES/sdk.js#xfbml=1&version=v3.2&appId=1492962730824222&autoLogAppEvents=1';
                                                        fjs.parentNode.insertBefore(js, fjs);
                                                    }(document, 'script', 'facebook-jssdk'));</script>
                                                <div class="fb-like" data-href="https://www.anime.cr/anime/{{$Anime->slug}}" data-colorscheme="dark" data-layout="button_count" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
                                            </li>
                                        </ul>
                                    </div>

                                    <ul class="card__meta">
                                        <li><span>Genero:</span>
                                            @foreach($Anime->Categories as $categories)
                                                <a>{{$categories->category}}</a>
                                        @endforeach
                                        <li>
                                    </ul>
                                    <div class="card__description card__description--details">
                                        {{$Anime->sinopsis}}
                                    </div>
                                </div>
                            </div>
                            <!-- end card content -->
                        </div>
                    </div>
                </div>
                <!-- end content -->

                <div class="col-12 col-xl-6">
                    <video controls playsinline  poster="{{asset('img/player.jpg')}}" id="player" width="540"
                           height="303">
                        <source id="mp4video" src="" type="video/mp4">
                        @if($Anime->vtt > 0)
                        <track kind="captions" id="subs" label="Español" srclang="es" src="" >
                        @endif
                    </video>
                </div>
                <!-- end player -->
                <!-- accordion -->
                <div class="col-12 col-xl-6">
                    <div class="accordion" id="accordion">

                        @foreach($Anime->charters->sortByDesc("id") as $cap)

                            <div class="accordion__card">
                                <div class="card-header" id="headingOne">
                                    <button type="button" data-toggle="collapse" data-target="#collapse{{$cap->id}}"
                                            aria-expanded="false" aria-controls="collapse{{$cap->id}}">
                                        <span>Episodio: {{$cap->cap}}</span>
                                    </button>
                                </div>
                                <div id="collapse{{$cap->id}}" class="collapse" aria-labelledby="heading{{$cap->id}}"
                                     data-parent="#accordion">
                                    <div class="card-body">
                                        <div class="panel pink">
                                            @foreach($cap->server as $item)
                                                <button type="button" class="button"
                                                        onclick="playVideo('{{$item["pivot"]->code}}','{{$item["pivot"]->vtt_source}}')">
                                                    {{$item->alias}}
                                                </button>

                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <!-- end accordion -->
                <div class="col-12">
                    <div id="fb-root"></div>
                    <script>(function(d, s, id) {
                            var js, fjs = d.getElementsByTagName(s)[0];
                            if (d.getElementById(id)) return;
                            js = d.createElement(s); js.id = id;
                            js.src = 'https://connect.facebook.net/es_ES/sdk.js#xfbml=1&version=v3.2&appId=1977598625883047&autoLogAppEvents=1';
                            fjs.parentNode.insertBefore(js, fjs);
                        }(document, 'script', 'facebook-jssdk'));</script>
                    <div class="fb-comments" data-colorscheme="dark" data-href="https://www.crani.me/anime/{{$Anime->slug}}" data-numposts="5"></div>
                </div>

            </div>
        </div>
        <!-- end details content -->
    </section>
    <!-- end details -->
@endsection
@section("styles")
    <link rel="stylesheet" href="{{asset("css/botones.css")}}">
@endsection
@section("player")
    <script>

        var videoID = 'player';
        var sourceID = 'mp4video';
        var subsID = 'subs';

        function playVideo(url,vtt) {

            $('#'+videoID).get(0).pause();
            $('#'+sourceID).attr('src', url);
            if (vtt != null)
            {
                $('#'+subsID).attr('kind', 'captions');
                $('#'+subsID).attr('src', vtt);
            }
            $('#'+videoID).get(0).load();
            //$('#'+videoID).attr('poster', newposter); //Change video poster
            $('#'+videoID).get(0).play();
        }

    </script>
@endsection
