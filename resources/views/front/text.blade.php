@extends("layouts.flixgo")
@section("title")
    {{$Title}}
@endsection
@section("content")
    <!-- page title -->
    <section class="section section--first section--bg"
             data-bg="{{route("images", str_replace("\\","*",setting('site.catimg')))}}">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section__wrap">
                        <!-- section title -->
                        <h2 class="section__title">{{$Title}}</h2>
                        <!-- end section title -->

                        <!-- breadcrumb -->
                        <ul class="breadcrumb">
                            <li class="breadcrumb__item"><a href="{{route("index")}}">Inicio</a></li>
                            <li class="breadcrumb__item breadcrumb__item--active"> {{$Title}}</li>
                        </ul>
                        <!-- end breadcrumb -->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end page title -->

    <!-- about -->
    <section class="section">
        <div class="container">
            <div class="row">
                <!-- section title -->
                <div class="col-12">
                    <h2 class="section__title">{{$Title}}</h2>
                </div>
                <!-- end section title -->

                <!-- section text -->
                <div class="col-12">
                    @if($Title == "Terminos de uso")
                        <p class="section__text">{!! setting('site.terms') !!}</p>
                    @else
                        <p class="section__text">  {!! setting('site.privacy') !!} </p>
                    @endif
                </div>
                <!-- end section text -->
            </div>
        </div>
    </section>
    <!-- end about -->
@endsection
