@extends("layouts.flixgo")
@section("title")
    Resultado de busqueda
@endsection
@section("content")
    <!-- page title -->
    <section class="section section--first section--bg" data-bg="{{route("images", str_replace("\\","*",setting('site.catimg')))}}">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section__wrap">
                        <!-- section title -->
                        <h2 class="section__title">Resultados</h2>
                        <!-- end section title -->

                        <!-- breadcrumb -->
                        <ul class="breadcrumb">
                            <li class="breadcrumb__item"><a href="{{route("index")}}">Inicio</a></li>
                            <li class="breadcrumb__item breadcrumb__item--active">Resultado de busqueda</li>
                        </ul>
                        <!-- end breadcrumb -->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end page title -->

    <!-- catalog -->
    <div class="catalog">
        <div class="container">
            <div class="row">
            @if($Animes != null)
            @foreach($Animes as $item)
                <!-- card -->
                    <div class="col-6 col-sm-4 col-lg-3 col-xl-2">
                        <div class="card">
                            <div class="card__cover">
                                <img src="{{route("images", str_replace("\\","*",$item->img))}}" alt="{{$item->name}}">
                                <a href="{{route("anime",$item->slug)}}" class="card__play">
                                    <i class="icon ion-ios-play"></i>
                                </a>
                            </div>
                            <div class="card__content">
                                <h3 class="card__title"><a href="{{route("anime",$item->slug)}}">{{$item->name}}</a></h3>
                                <span class="card__rate"><i class="icon ion-ios-star"></i>8.4</span>
                                <ul class="card__list">
                                    <li>{{$item->state > 0 ? "Finalizado" : "Emision"}}</li>
                                    <li>{{$item->Type->type}}</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- end card -->
            @endforeach
            <!-- paginator -->
            {{$Animes->onEachSide(1)->links("layouts.parts.paginator")}}
            <!-- end paginator -->
            @else
                    <h2 class="section__title">No se encontraron considencias</h2>
            @endif
            </div>
        </div>
    </div>
    <!-- end catalog -->
@endsection
