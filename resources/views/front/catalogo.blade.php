@extends("layouts.flixgo")
@section("title")
    Lista de Animes
@endsection
@section("content")
    <!-- page title -->
    <section class="section section--first section--bg"
             data-bg="{{route("images", str_replace("\\","*",setting('site.catimg')))}}">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section__wrap">
                        <!-- section title -->
                        <h2 class="section__title">Lista de Animes</h2>
                        <!-- end section title -->

                        <!-- breadcrumb -->
                        <ul class="breadcrumb">
                            <li class="breadcrumb__item"><a href="{{route("index")}}">Inicio</a></li>
                            <li class="breadcrumb__item breadcrumb__item--active">Lista de Animes</li>
                        </ul>
                        <!-- end breadcrumb -->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end page title -->

    <!-- filter -->
    <div class="filter">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="filter__content">
                        <form action="{{route("catalog")}}" method="GET" id="Filter">
                            <div class="filter__items">
                                <!-- filter item -->
                                <div class="filter__item" id="filter__genre">
                                    <span class="filter__item-label">Genero:</span>

                                    <div class="filter__item-btn dropdown-toggle" role="navigation" id="filter-genre"
                                         data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <input type="button" name="genero" value="{{$Genero}}">
                                        <span></span>
                                    </div>

                                    <ul class="filter__item-menu dropdown-menu scrollbar-dropdown"
                                        aria-labelledby="filter-genre">
                                        <li>Todas</li>
                                        @foreach($Generos as $item)
                                            <li data-value="{{$item->id}}">{{$item->category}}</li>
                                        @endforeach


                                    </ul>
                                </div>
                                <!-- end filter item -->

                                <!-- filter item -->
                                <div class="filter__item" id="filter__type">
                                    <span class="filter__item-label">Tipo:</span>

                                    <div class="filter__item-btn dropdown-toggle" role="navigation" id="filter-type"
                                         data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <input type="button" name="tipo" value="{{$Tipo}}">
                                        <span></span>
                                    </div>

                                    <ul class="filter__item-menu dropdown-menu scrollbar-dropdown"
                                        aria-labelledby="filter-type">
                                        <li>Todos</li>
                                        @foreach($Tipos as $item)
                                            <li>{{$item->type}}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                <!-- end filter item -->
                                <!-- filter item -->
                                <div class="filter__item" id="filter__status">
                                    <span class="filter__item-label">Estado:</span>

                                    <div class="filter__item-btn dropdown-toggle" role="navigation" id="filter-status"
                                         data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <input type="button" name="estado" value="{{$Estado}}">
                                        <span></span>
                                    </div>

                                    <ul class="filter__item-menu dropdown-menu scrollbar-dropdown"
                                        aria-labelledby="filter-status">
                                        <li>Todos</li>
                                        <li>Emisión</li>
                                        <li>Concluido</li>

                                    </ul>
                                </div>
                                <!-- end filter item -->
                            </div>
                            <input type="hidden" value="#" id="state" name="state">
                            <input type="hidden" value="#" id="gender" name="gender">
                            <input type="hidden" value="#" id="type" name="type">
                        </form>
                        <button class="filter__btn" id="send" onclick="filter()" type="button">Filtrar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end filter -->

    <!-- catalog -->
    <div class="catalog">
        <div class="container">
            <div class="row">
                @foreach($Animes as $item)
                    <!-- card -->
                        <div class="col-6 col-sm-4 col-lg-3 col-xl-2">
                            <div class="card">
                                <div class="card__cover">
                                    <img src="{{route("images", str_replace("/","*",$item->img))}}"
                                         alt="{{$item->name}}">
                                    <a href="{{route("anime",$item->slug)}}" class="card__play">
                                        <i class="icon ion-ios-play"></i>
                                    </a>
                                </div>
                                <div class="card__content">
                                    <h3 class="card__title"><a href="{{route("anime",$item->slug)}}">{{$item->name}}</a>
                                    </h3>
                                    <span class="card__rate"><i class="icon ion-ios-star"></i>8.4</span>
                                    <ul class="card__list">
                                        <li>{{$item->state > 0 ? "Finalizado" : "Emision"}}</li>
                                        <li>{{$item->Type->type}}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- end card -->
                @endforeach
                <!-- paginator -->
                {{$Animes->appends(["state"=>$Estado,"gender"=>$Genero,"type"=>$Tipo])->onEachSide(1)->links("layouts.parts.paginator")}}
                <!-- end paginator -->
            </div>
        </div>
    </div>
    <!-- end catalog -->
@endsection
@section("player")
    <script>
        function filter() {
            var gender = $('input[name = "genero"]').val();
            var type = $('input[name = "tipo"]').val();
            var state = $('input[name = "estado"]').val();
            $(document).ready(function () {
                $("#send").click(function () {
                    document.getElementById("type").value = type;
                    document.getElementById("gender").value = gender;
                    document.getElementById("state").value = state;
                    $("#Filter").submit();
                });
            });
        }


    </script>
@endsection
