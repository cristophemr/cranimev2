<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Font -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600%7CUbuntu:300,400,500,700" rel="stylesheet">
<!-- CSS -->

<link rel="stylesheet" href="{{asset("css/bootstrap-reboot.min.css")}}">
<link rel="stylesheet" href="{{asset("css/bootstrap-grid.min.css")}}">
<link rel="stylesheet" href="{{asset("css/owl.carousel.min.css")}}">
<link rel="stylesheet" href="{{asset("css/jquery.mCustomScrollbar.min.css")}}">
<link rel="stylesheet" href="{{asset("css/nouislider.min.css")}}">
<link rel="stylesheet" href="{{asset("css/ionicons.min.css")}}">
<link rel="stylesheet" href="{{asset("css/plyr.css")}}">
<link rel="stylesheet" href="{{asset("css/photoswipe.css")}}">
<link rel="stylesheet" href="{{asset("css/default-skin.css")}}">
<link rel="stylesheet" href="{{asset("css/main.css")}}">
@yield("styles")

{{--<!-- Favicons -->--}}
{{--<link rel="icon" type="image/png" href="icon/favicon-32x32.png" sizes="32x32">--}}
{{--<link rel="apple-touch-icon" href="icon/favicon-32x32.png">--}}
{{--<link rel="apple-touch-icon" sizes="72x72" href="icon/apple-touch-icon-72x72.png">--}}
{{--<link rel="apple-touch-icon" sizes="114x114" href="icon/apple-touch-icon-114x114.png">--}}
{{--<link rel="apple-touch-icon" sizes="144x144" href="icon/apple-touch-icon-144x144.png">--}}

{{--SEO--}}
@if(Route::current()->getName() === 'index')
    <meta name="description" content="{!! setting('site.description') !!}">
    <meta name="keywords" content="{!! setting('site.keywords') !!}">
    <meta name="author" content="{!! setting('site.author') !!}">
@else
    @yield("meta")
@endif

<title> @if(Route::current()->getName() === 'index'){{setting('site.title')}} @else @yield("title")
    - {{setting('site.title')}} @endif</title>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-132741051-1"></script>
{{setting('site.google_analytics_tracking_id')}}
