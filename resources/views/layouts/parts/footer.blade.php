<footer class="footer">
    <div class="container">
        <div class="row">
            {{--<!-- footer list -->--}}
            <div class="col-12 col-md-3">
                <h6 class="footer__title">Descarga nuestra app</h6>
                <ul class="footer__app">
                    <li> <h7 class="footer__title">Proximamente!!</h7></li>
                    <li><a><img src="{{asset("img/Download_on_the_App_Store_Badge.svg")}}" alt="IOS"></a></li>
                    <li><a><img src="{{asset("img/google-play-badge.png")}}" alt="Android"></a></li>
                </ul>
            </div>
            {{--<!-- end footer list -->--}}

            {{--<!-- footer list -->--}}
            <div class="col-6 col-sm-4 col-md-3">
                <h6 class="footer__title">Facebook</h6>
                <ul class="footer__list">
                    {{--<li><a href="#">Terms of Use</a></li>--}}
                    {{--<li><a href="#">Privacy Policy</a></li>--}}
                    {{--<li><a href="#">Security</a></li>--}}
                    <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FCrAnimeOficial&tabs&width=340&height=130&small_header=false&adapt_container_width=false&hide_cover=false&show_facepile=false&appId=1492962730824222" width="350" height="130" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
                </ul>
            </div>
            {{--<!-- end footer list -->--}}
        <!-- footer list -->
            <div class="col-6 col-sm-4 col-md-3">
                {{--<h6 class="footer__title">Resources</h6>--}}
                {{--<ul class="footer__list">--}}
                {{--<li><a href="#">About Us</a></li>--}}
                {{--<li><a href="#">Pricing Plan</a></li>--}}
                {{--<li><a href="#">Help</a></li>--}}
                {{--</ul>--}}
            </div>
            {{--<!-- end footer list -->--}}


            {{--<!-- footer list -->--}}
            <div class="col-12 col-sm-4 col-md-3">
                <h6 class="footer__title">Contacto</h6>
                <ul class="footer__list">
                    {{--<li><a href="tel:+18002345678">+1 (800) 234-5678</a></li>--}}
                    <li><a href="mailto:contenidos@crani.me">contenidos@crani.me</a></li>
                </ul>
                <ul class="footer__social">
                    <li class="facebook"><a href="https://www.facebook.com/CrAnimeOficial" target="_blank"><i class="icon ion-logo-facebook"></i></a></li>
                    {{--<li class="instagram"><a href="#"><i class="icon ion-logo-instagram"></i></a></li>--}}
                    {{--<li class="twitter"><a href="#"><i class="icon ion-logo-twitter"></i></a></li>--}}
                    {{--<li class="vk"><a href="#"><i class="icon ion-logo-vk"></i></a></li>--}}
                </ul>
            </div>
            <!-- end footer list -->

            <!-- footer copyright -->
            <div class="col-12">
                <div class="footer__copyright">
                    <small>© @php echo date("Y"); @endphp {{config("app.name")}}. Desarrollado por <a href="https://www.econsulting.cr" target="_blank">Econsulting</a></small>

                    <ul>
                        <li><a href="{{url("terminos")}}">Terminos de uso</a></li>
                        <li><a href="{{url("privacidad")}}">Politicas de privacidad</a></li>
                    </ul>
                </div>
            </div>
            <!-- end footer copyright -->
        </div>
    </div>
</footer>
