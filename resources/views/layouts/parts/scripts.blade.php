<script src="{{asset("js/jquery-3.3.1.min.js")}}"></script>
<script src="{{asset("js/bootstrap.bundle.min.js")}}"></script>
<script src="{{asset("js/owl.carousel.min.js")}}"></script>
<script src="{{asset("js/jquery.mousewheel.min.js")}}"></script>
<script src="{{asset("js/jquery.mCustomScrollbar.min.js")}}"></script>
<script src="{{asset("js/wNumb.js")}}"></script>
<script src="{{asset("js/nouislider.min.js")}}"></script>
<script src="{{asset("js/plyr.min.js")}}"></script>
<script src="{{asset("js/jquery.morelines.min.js")}}"></script>
<script src="{{asset("js/photoswipe.min.js")}}"></script>
<script src="{{asset("js/photoswipe-ui-default.min.js")}}"></script>
<script src="{{asset("js/main.js")}}"></script>
@yield("player")
{{--<script>--}}
    {{--$(document).ready(function(){--}}

        {{--$('#anime').keyup(function(){--}}
            {{--var query = $(this).val();--}}
            {{--if(query != '')--}}
            {{--{--}}
                {{--var _token = $('input[name="_token"]').val();--}}
                {{--$.ajax({--}}
                    {{--url:"{{ route('search') }}",--}}
                    {{--method:"POST",--}}
                    {{--data:{query:query, _token:_token},--}}
                    {{--success:function(data){--}}
                        {{--$('#AnimeList').fadeIn();--}}
                        {{--$('#AnimeList').html(data);--}}
                    {{--}--}}
                {{--});--}}
            {{--}--}}
        {{--});--}}

        {{--$(document).on('click', 'li', function(){--}}
            {{--$('#AnimeList').val($(this).text());--}}
            {{--$('#AnimeList').fadeOut();--}}
        {{--});--}}

    {{--});--}}
{{--</script>--}}

