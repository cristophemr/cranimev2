<header class="header">
    <div class="header__wrap">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="header__content">
                        <!-- header logo -->
                        <a href="{{route("index")}}" class="header__logo">
                            <img src="{{route("images",str_replace("/","*",setting('site.logo'))) }}" alt="">
                        </a>
                        <!-- end header logo -->

                        <!-- header nav -->
                        <ul class="header__nav">
                            {!! menu('Site','layouts.parts.menu') !!}
                        </ul>
                        <!-- end header nav -->

                        <!-- header auth -->
                        <div class="header__auth">
                            <button class="header__search-btn" type="button">
                                <i class="icon ion-ios-search"></i>
                            </button>

                            <a href="#" class="header__sign-in">
                                <i class="icon ion-ios-log-in"></i>
                                <span>Ingresar</span>
                            </a>
                        </div>
                        <!-- end header auth -->

                        <!-- header menu btn -->
                        <button class="header__btn" type="button">
                            <span></span>
                            <span></span>
                            <span></span>
                        </button>
                        <!-- end header menu btn -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- header search -->
    <form action="{{route("search")}}" method="POST" class="header__search">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="header__search-content">
                            <input type="text" name="media" placeholder="Buscar . . . .">
                            {{ csrf_field() }}
                            <button type="submit">Buscar</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!-- end header search -->
    {{--<!-- header search -->--}}
    {{--<form action="#" class="header__search">--}}
        {{--<div class="container">--}}
            {{--<div class="row">--}}
                {{--<div class="col-12">--}}
                    {{--<div class="header__search-content">--}}
                            {{--<div class="filter__item-btn dropdown-toggle" role="navigation" id="filter-anime" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">--}}
                                {{--<input type="text" id="anime" placeholder="Buscar anime . . . .">--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--{{ csrf_field() }}--}}
                    {{--</div>--}}
            {{--<div>--}}
                {{--<ul class="dropdown header__nav-item" >--}}
                    {{--<li class="header__nav-item"> TExTO LOREM</li>--}}
                {{--</ul>--}}
            {{--</div>--}}

                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</form>--}}
    {{--<!-- end header search -->--}}
</header>
