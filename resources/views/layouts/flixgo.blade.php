<!DOCTYPE html>
<html lang="en">
<head>
    @includeIf("layouts.parts.header")
</head>
<body class="body">

<!-- header -->
@includeIf("layouts.parts.HtmlHeader")
<!-- end header -->

<!-- home -->
 @yield("slider")
<!-- end home -->

<!-- content -->
<section class="content">
    @yield("content")
</section>
<!-- end content -->

<!-- footer -->
@includeIf("layouts.parts.footer")
<!-- end footer -->

<!-- JS -->
@includeIf("layouts.parts.scripts")
</body>
</html>
